function primaryclick(e) {
  $('.toast').toast('show');
}

function countRabbits() {
  for (var i = 1; i <= 3; i++) {
    alert('Кролик номер ' + i);
  }
}

function onscroll(e, scroll) {
  console.log(e, scroll);
}

window.onload = function() {
  var btns = document.getElementsByClassName('btn');
  /* for (var i = 0; i < btns.length; i++) {
    //btns[i].onclick = primaryclick;
    btns[i].addEventListener('click', primaryclick);
    btns[i].removeEventListener('click', primaryclick);
  } */
  btns[0].addEventListener('click', primaryclick);
  btns[0].addEventListener('click', countRabbits);

  btns[2].addEventListener('click', function(e) {
    btns[0].removeEventListener('click', primaryclick);
  });
  btns[1].addEventListener('click', function(e) {
    window.removeEventListener('scroll', onscroll);
  });
};

window.addEventListener('scroll', onscroll);
